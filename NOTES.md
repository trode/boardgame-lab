**https://github.com/kitsonk/deno_ls_plugin** allows VSCode to works fine with deno imports (i.e. *.ts). Don't forget to select the workspace version of typescript !

`export DENO_DIR=$HOME/projects/boardgame-lab/deno` to cache remote imports directly inside the project (then update the package.ts file to search files inside ./deno/deps/...).