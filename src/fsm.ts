import { State, StateId, Execution } from './state/state';

export type execution = {state: StateId, action: string}
export type executionStack = execution[]

/**
 * FSM : Finite State Machine
 *
 * Interaction with states use StateId (except for adding the states to the fsm, of course),
 * to avoid to mess with object references.
 * For now, those are just string, but it could change.
 *
 */
class FSM {

    // /**
    //  * The lifo list of executions
    //  */
    // executionStack: {state: StateId, execution: string}[] = []

    /**
     * All states, except the initial one
     */
    states: Map<StateId, State> = new Map()

    constructor(states: Map<StateId, State> = new Map()) {
        this.addStates(Array.from(states.values()))
    }

    /**
     * Run the fsm throw executions until it reaches a 'self' execution
     */
    solve(state: StateId, executionStack: executionStack): {newState: StateId, newExecutionStack: executionStack} {
        // TODO: en fait pas besoin d'avoir une classe, juste une methode de game suffit


        // go to the choosen state
        // let currentState: StateId = choice
        // load the execution list

        
        let action: StateId
        
        do {
            this.states.get(state)!.executions.forEach(e => executionStack.push({state: state, action: e}))
            // currentState = execution
            ;({state, action} = executionStack.pop()!)

            // execution = executionStack.pop()!
            // state = execution.state
        } while (action !== 'self');


        return {
            newState: state,
            newExecutionStack: executionStack
        }
    }

    //--------------------------------------------------------------------------
    // Initialize
    //--------------------------------------------------------------------------

    /**
     * Add a state, throw error if the fsm allready have a state with this id
     */
    addState(state: State): void {
        if (this.states.get(state.id) !== undefined) {
            throw 'Already assigned state'
        }
        this.states.set(state.id, state)
    }

    /**
     * Add multiple states
     * @param states 
     */
    addStates( states: State[] ): void {
        states.forEach(s => {
            this.addState(s)
        })
    }
}


export { FSM };
