import { FSM, execution, executionStack } from './fsm'
import { GameConfig } from './game-config'
import * as S from './state/state'

/**
 * What's come in
 */
export type GameInput = {name: string, value: string|number}

/**
 * Le retour de la résolution d'un état instable.
 *  Donne le nom de l'état,
 * et une valeur numérique arbitraire pour le moment (mais sera un delta de l'état du jeu)
 */
type instableStateResult = {
    stateName: string,
    result: number,
}

/**
 * What's come out
 */
export type GameResponse = {
    instableStateResults: instableStateResult[],
    currentState: string,
}

export type GameResponseError = {
    message: string
}

/**
 * A game
 */
export class GameLab {
    config: GameConfig = new GameConfig()
    states: Map<S.StateId, S.State> = new Map()
    currentState: S.State|undefined
    hadInitialState: boolean = false
    executionStack: executionStack = []

    init() {
        // this.buildFsm()
        this.currentState = this.getInitialState()
    }

 

    /**
     * Faisable uniquement depuis un StableState
     * @param input 
     */
    handleInput( input: GameInput ): GameResponse {

        if ( input.name.length < 1 || input.value < 1 ) {
            throw {message: 'invalid name or value'}
        }

        // let avaibleActions: string[] = ['necrose', 'soin', 'onde']

        // if ( ! avaibleActions.includes( input.name ) ) {
        //     throw {message: 'unexistant action'}
        // }
        
        let action: S.StateId = input.name
        // TODO: action accessible à partir de l'état actuel

        if ( ! (<S.StableState>this.currentState).options.map( o => o.name ).includes( action ) ) {
            throw {message: 'unaccessible action'}
        }

        let fsm: FSM = new FSM(this.states) // TODO: en faire une méthode de game, virer fsm
        let {newState, newExecutionStack} = fsm.solve(action, this.executionStack)
        this.currentState = <S.State>this.states.get(newState)
        this.executionStack = newExecutionStack
        let result = this.execute(this.currentState) // -> va modifier la config, et retourner des choses à traiter par l'UI, + faire des logs, etc
        
        // TODO: comment un état va pouvoir passer des infos à un autre ?
        // car après tout c'est le but de cette notion d'execution, c'est qu'un état puisse avoir besoin de choses venues d'un autre,
        // par exemple une action va avoir besoin d'un résultat de jet de dé
        // on récupère ça dans le result, ce qu'il faudra définir mais qui va être assez varié,
        // et il faudrait l'envoyer dans le prochain this.execute,
        // mais pour ça il faut boucler ici pour se déplacer jusqu'à ce qu'on soit à un UserState
        // et quand bien même il va peut être falloir stocker des résultats intermédiaires ... ?
        // Peut être on peut fourrer ça dans la pile d'execution ...

        // console.log( inputs )
        return {
            instableStateResults: [],
            currentState: this.currentState.id,
        }

    }

    execute(state: S.State) {
        console.log(state.id)
    }

    //--------------------------------------------------------------------------
    // States
    //--------------------------------------------------------------------------
    
    /**
     * Add a state, throw error if the fsm allready have a state with this id
     */
    addState(state: S.State) {
        if (this.hadInitialState) {
            throw 'FSM allready have an initial state';
        }
        if (this.states.get(state.id) !== undefined) {
            throw 'Already assigned state'
        }
        this.states.set(state.id, state)
        if (state instanceof S.InitialState) {
            this.hadInitialState = true
        }
    }

    /**
     * Add multiple states
     * @param states 
     */
    addStates( states: S.State[] ) {
        states.forEach(s => {
            this.addState(s)
        })
    }

    getInitialState(): S.InitialState {
        if (! this.hadInitialState) {
            throw 'FSM does not have an initial state'
        }
        return <S.InitialState>Array.from(this.states.values()).find( x => x instanceof S.InitialState)
    }
}
