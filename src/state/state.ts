

/**
 * Will surely change
 * Must be unique in a given fsm
 */
export type StateId = string

/**
 * Things in the execution stack of a System state :
 * The states who will be executed as child states (those states could have an execution list themselves)
 */
export type Execution = StateId|'self'

/**
 * The list of avaible actions from a StableState
 */
export type Options = {name: string, state: StateId};

/**
 * Base class for states
 */
export abstract class State {
    executions = ['self']
    constructor( public id: StateId ) {}
}

/**
 * User State : the fsm is waiting an input, this input should be in Options
 */
export abstract class UserState extends State {

}

/**
 * State state : state where the game can be really 'paused' :
 * no memory is needed other than the id of the current stable state
 * and the game config (no execution, genealogy, etc)
 */
export class StableState extends UserState {
    options: Options[] = [];
}


/**
 * A user state between System states,
 * used as a prompt for the resolution of an execution
 * (exemple : roll dice, choose to play a card in response of an attack)
 */
export class UnstableState extends UserState {

}


/**
 * System state : the fsm is moving accross the Executions
 * until it reach a UserState or a FinalState
 */
export class SystemState extends State {
    executions: Execution[] = [];
}

/**
 * Initial state : need to be unique for a given fsm
 */
export class InitialState extends SystemState {}

/**
 * Final state : End of the game, can be multiple for a given fsm
 */
export class FinalState extends SystemState {}
