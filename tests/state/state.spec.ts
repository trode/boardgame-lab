import { StableState } from '../../src/state/state';
import { expect } from 'chai';
import 'mocha';

describe('Stable State', () => {
    it('should have an id', () => {
        let stableState: StableState = new StableState('id')
        expect(stableState.id).to.equal('id')
    })

})
