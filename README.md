# boardgame-lab
A boardgame implementation framework

## Définitions

### FSM :
Finite State Machine (machine à état fini ou automate fini). Une FSM est définie par un état initial, un ou des états finaux, et un ensemble d'état stables et d'états instables. L'ensemble constitue un graphe orienté connexe.
La fsm n'a pas de mémoire, elle existe seulement pour être interrogée. Ses états ont une liste d'execution par défaut qui pourrat être surchargée par la configuration de la partie (exemple : un pouvoir sur un personnage "si 3+ Blessures, annulez en une", implique une execution supplémentaire lors de toute action qui inflige des blessures).
Pour interroger la FSM, on doit lui fournir :
    - l'état de départ
    - l'action soumlise
    - la généalogie actuelle (vide par défaut)
Elle répond :
    - le nouvel état atteint (elle peut en parcourir plusieurs, elle s'arrête dès qu'elle atteint une execution "self")
    - la nouvelle généalogie

Par défaut, une fsm s'interroge à partir d'un état utilisateur, mais on pourra imaginer une api pour interroger à partir de n'importe quel état (ce qui sera nécessaire pour tests)
La Game possède les états, et les soumets à une instance de Fsm lorsque besoin
// TODO: Est ce que la fsm a besoin de différencier les notions de InitialState, FinalState, UserState, etc ? En soit, elle a juste besoin de recevoir des states avec leurs executions
je pense que non, la fsm est "aveugle", elle sert juste à se déplacer dans les états et leurs executions, afin de dire à la game ce qu'elle doit à présent traiter

### State :
Un état est un noeud du graphe FSM.

### Initial State :
**Unique** et **Obligatoire**. Un seul arc, sortant.

### Final State :
**Obligatoire** (au moins un). Aucun arc sortant.

### User State :
Noeud correspondant à une attente d'input de la part d'un joueur. Le jeu étant bloqué tant que le joueur concerné n'agit pas, on pourra mettre en place un système de timer pour forcer une sortie de l'état en l'absence d'action du joueur.

#### Stable State :
Un User State dit "Stable" est un état où la généalogie est vide. C'est un état ou le jeu n'attends rien d'autres qu'une action d'un joueur, et qui n'a rien en cours de résolution. Typiquement, c'est des états où le jeu peut être "mis en pause" : il suffit de sauvegarder la configuration de la partie et le noeud courant pour avoir tout en main.

#### Unstable State :
Un User State dit "Instable" est un état où la généalogie n'est pas vide, et donc où le système est en attente d'un input pour résoudre une execution stack.

### System State :
Noeud correspondant à une étape de résolution de la part du système. Aucun joueur ne peut agir dans un état instable. Un état instable possède une liste d'exécutions qui détaillent son fonctionnement.

### State Executions :
Un System State dispose d'une liste ordonnée d'executions, qui indique quelles étapes sont nécessaire pour résoudre cet état. Cette liste doit comprendre la résolution "self" (mot-clef à discuter, peut changer), mais "self" peut être précédé ou suivi par d'autres résolutions, qui consistent alors en une référence à un autre état.

### Genealogy :
Les système de généalogie permet de mettre en suspend la résolution d'un état afin de résoudre d'abord des états-fils. En ajoutant un état dans la généalogie, on crée un point d'ancrage pour le système qui, lorsque tous les états fils seront résolus, reviendra automatiquement au dernier état-parent à résoudre. Par exemple, une action A requiert qu'un joueur lance un dé afin d'exploiter le résultat ce dé pour être résolue ("lancez un dé et piochez autant de cartes"). L'action A est un état instable qui nécissite l'utilisation d'un état stable "lancé de dé". L'action A va donc devenir un état-parent en créant une généalogie afin qu'une fois l'état "lancé de dé" résolu, on remonte automatiquement le fil et exploite ce résultat de dé.